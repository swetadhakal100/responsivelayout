import "./App.css";

function App() {
  return (
    <div className="heroContainer">
      <div className="heroBody">
        <div className="heroItems">
          <h1>Responsive layouts don't have to be a struggle</h1>
          <p>
            Don't let distance silence your conversations. Recharge their phones
            instantly with Hamro Recharge and stay connected, no matter where
            you are!Our service is compatible with all major Nepali mobile
            networks, including NTC & Ncell. We offer a variety of recharge
            options.
          </p>
          <button>I want to learn</button>
        </div>

        <div className="imageBody">
          <div className="imageContainer">
            <img src="./image 18.jpg" alt="image18" />
            <h1>Reliability</h1>
            <p>
              Hamro Patro has established itself as a trusted platform for
              Nepalese both at home and abroad. Our service guarantees that your
              mobile recharge reaches its destination quickly and without any
              hassle.
            </p>
          </div>
          <div className="imageContainer">
            <img src="./image 19.jpg" alt="image19" />
            <h1>Convenience</h1>
            <p>
              With Hamro Patro, sending a recharge is as simple as a few clicks.
              Whether you’re at work, home, or on the move, our platform is
              accessible 24/7, making it incredibly convenient to send a
              recharge anytime, anywhere.
            </p>
          </div>
          <div className="imageContainer">
            <img src="./image 20.jpg" alt="image20" />
            <h1>Speed</h1>
            <p>
              We understand the importance of time. That’s why our system is
              designed for rapid processing, ensuring that your recharge is
              delivered almost instantly.
            </p>
          </div>
        </div>

        <div className="fastestWay">
          <h1>The fastest way to send mobile top-up worldwide</h1>
          <p>
            Whether you call it top-up, mobile recharge, reload, load or credit,
            we've got you covered. We've delivered over 500 million
            international mobile recharges online.
          </p>
          <button>Send Recharge Now</button>
        </div>

        <div className="sendRechargeContainer">
          <div className="sendRecharge">
            <img src="./rechargeProcess.jpg" alt="rechargeProcessImg" />
          </div>
          <div className="sendRechargeText">
            <h1>How to Send Recharge?</h1>
            <p>
              Looking for an affordable way to recharge?  Choose Hamro Recharge!
              We are committed to providing you with the lowest fees possible.
            </p>
            <button>Send Recharge Now</button>
          </div>
        </div>
      </div>
    </div>

    // <div className="parent">
    //   <div className="child"></div>
    // </div>
    // <div className="container">
    //   <p>My name is sweta dhakal</p>
    //   <p>
    //     In publishing and graphic design, Lorem ipsum (/ˌlɔː.rəm ˈɪp.səm/) is a
    //     placeholder text commonly used to demonstrate the visual form of a
    //     document or a typeface without relying on meaningful content. Lorem
    //     ipsum may be used as a placeholder before final copy is available.
    //     Wikipedia
    //   </p>
    // </div>

    // <div>
    //   <div className="intro">
    //     <div className="container">
    //       <div className="intro-content">
    //         <h1>My name is sweta dhakal</h1>
    //         <p>
    //           In publishing and graphic design, Lorem ipsumɪp.səm is a
    //           placeholder text commonly used to demonstrate the visual form of a
    //           document or a typeface without relying on meaningful content.
    //           Lorem ipsum may be used as a placeholder before final copy is
    //           available. Wikipedia
    //         </p>
    //       </div>
    //     </div>
    //   </div>

    //   <div className="container">
    //     <h2>More content</h2>
    //     <p>
    //       Lorem Ipsum is simply dummy text of the printing and typesetting
    //       industry. Lorem Ipsum has been the industry's standard dummy text ever{" "}
    //     </p>
    //     <p>
    //       since the 1500s, when an unknown printer took a galley of type and
    //       scrambled typeface without relying on meaningful content. Lorem ipsum
    //       may be used as
    //     </p>
    //     <p>
    //       Lorem ipsum may be used as a placeholder before final copy is
    //       available. Wikipedia typeface without relying on meaningful content.
    //       Lorem ipsum may be used as
    //     </p>
    //     <p>
    //       since the 1500s, when an unknown printer took a galley of type and
    //       scrambled typeface without relying on meaningful content. Lorem ipsum
    //       may be used as
    //     </p>
    //   </div>
    // </div>

    // <div className="containers">
    //   <div className="containersPara">
    //     <h1>Responsive layouts don't have to be a struggle</h1>
    //     <p>
    //       In publishing and graphic design, Lorem ipsum is a placeholder text
    //       commonly used to demonstrate the visual form of a document or a
    //       typeface without relying on meaningful content. Lorem ipsum may be
    //       used as a placeholder before final copy is available. Wikipedia
    //     </p>
    //     <button>I WANT TO LEARN</button>
    //   </div>

    //   <div className="grid-container">
    //     <div className="grid">
    //       <h2>Speed</h2>
    //       <p>
    //         Lorem ipsum is a placeholder text commonly used to demonstrate the
    //         visual form of a document or a typeface without relying on
    //         meaningful content. Lorem ipsum may be used as a placeholder before
    //         final copy is available. Wikipedia
    //       </p>
    //     </div>
    //     <div className="grid">
    //       <h2>Convenience</h2>
    //       <p>
    //         Lorem ipsum is a placeholder text commonly used to demonstrate the
    //         visual form of a document or a typeface without relying on
    //         meaningful content. Lorem ipsum may be used as a placeholder before
    //         final copy is available. Wikipedia
    //       </p>
    //     </div>
    //     <div className="grid">
    //       <h2>Reliability</h2>
    //       <p>
    //         Lorem ipsum is a placeholder text commonly used to demonstrate the
    //         visual form of a document or a typeface without relying on
    //         meaningful content. Lorem ipsum may be used as a placeholder before
    //         final copy is available. Wikipedia
    //       </p>
    //     </div>
    //     <div className="grid">
    //       <h2>Speed</h2>
    //       <p>
    //         Lorem ipsum is a placeholder text commonly used to demonstrate the
    //         visual form of a document or a typeface without relying on
    //         meaningful content. Lorem ipsum may be used as a placeholder before
    //         final copy is available. Wikipedia
    //       </p>
    //     </div>
    //     <div className="grid">
    //       <h2>Convenience</h2>
    //       <p>
    //         Lorem ipsum is a placeholder text commonly used to demonstrate the
    //         visual form of a document or a typeface without relying on
    //         meaningful content. Lorem ipsum may be used as a placeholder before
    //         final copy is available. Wikipedia
    //       </p>
    //     </div>
    //     <div className="grid">
    //       <h2>Reliability</h2>
    //       <p>
    //         Lorem ipsum is a placeholder text commonly used to demonstrate the
    //         visual form of a document or a typeface without relying on
    //         meaningful content. Lorem ipsum may be used as a placeholder before
    //         final copy is available. Wikipedia
    //       </p>
    //     </div>
    //   </div>
    // </div>
  );
}

export default App;
